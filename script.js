console.log('test');
// Updates on the ES6
/*
1. Exponent Operator
*/
//before
const firstNum = 8 ** 2;
console.log(firstNum);
//update
const secondNum = Math.pow(8, 2);
console.log(secondNum);

/*
2. Template Literals (``)
- Allows to write strings without using the concatenation operator (+)
- Greatly helps with code readability
*/

let name = 'John';
//before
let message = 'Hello' + name + '! Welcome to programming!';
console.log("message without template literals: " +message);

//using template literals
message = `Hello ${name}! Welcome to programming!`;
console.log(`message with template literals: ${message}`);

//multi-line - no need for new line!
const anotherMessage = `


${name} attended a Math competition. He won it by solving 

the problem 8 ** 2 with the
solution of ${firstNum}`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

/*
3. Array Destructuring
- Allows to unpack elements in arrays into distinct variables
- Allows us to name array elements with variables instead of using index numbers
*/

const fullName = ["Juan","Dela","Cruz"];

//before
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//destructuring
const[firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

/*
4. Object Destructuring
-- allows to unpack properties of an object into distinct variables
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz",
	walk: function(){
		return `${this.gName} walked 1km.`
	}
};

//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

//using destructuring
const {givenName, maidenName, familyName, walk} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);


/* 5. arrow function
- compact alternative syntac to traditional functions
- useful for code snippets where creating functions will not be reused in any other portion of the code
- syntax:
const/let variableName = (parameter) => {statement};
*/

//before
const hello = function(){
	console.log("Hellow world!");
}

//using arrow function
const helloAgain = () => {
	console.log("Hello world!");
}

//Arrow functions with loops
const students = ["John", "Jane", "Judy"];

//before
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

//using arrow function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Problem with constructors for person
console.log(walk);
console.log(person.walk());
// Problem with constructors for person

/*
6. Implicit Return Statement
- There are some instances when you can omit the "return statement"
- JS implicitly adds it for the result of the function
*/

const add = (x, y) => x+y; 
// is the same with const add = (x, y) => {return x+y;}; but since there is only 1 statement, then it can be disregarded
// {} assumes that there is many statement in the function, with this, we needed to explicitly write return
let total = add(1,2);
console.log(total);

/*
7. Default function argument value
- provides a default argument value if none is provided
*/

const greet = (name = 'User') => {
	return `Good morning, ${name}!`;
}

console.log(greet());
console.log(greet('John'));

/*
8. Class-based object blueprint
- allows creation/instantiation of objects using classes as blueprints

Creating a class
- the constructor is a special method of a class for creating an object for that class
*/

// //before
// function Pokemon(name, level){
// 	this.name = name;
// 	this.level = level;
// 	this.health = 2 * level;
// 	this.attack = level;

// 	//methods
// 	this.tackle = function(target){
// 		console.log(this.name + ' tackled ' +target.name);
// 		console.log(target.name + "'s health is now reduced to _targetPokemonhealth_");
// 	},
// 	this.faint = function(){
// 		console.log(this.name+ ' fainted.');
// 	}
// }

// //after
// class Pokemon{
// 	constructor(name, level){

// 	}
// }
class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.year = year;
		this.name = name;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

